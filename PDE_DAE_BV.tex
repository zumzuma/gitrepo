\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[pdftex]{graphicx}	
\usepackage{pgfplots}

\newcommand{\todo}[1]{{\color{red}#1}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}

\newtheorem{thm}{Theorem}
\newtheorem{dfn}[thm]{Definition}
\newtheorem{exmp}[thm]{Example}
\newtheorem{rem}[thm]{Remark}
\newtheorem{ass}{Assumption}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}

\renewcommand{\AA}{\mathbf{A}}
\newcommand{\BB}{\mathbf{B}}
\newcommand{\CC}{\mathbf{C}}
\newcommand{\DD}{\mathbf{D}}
\newcommand{\EE}{\mathbf{E}}
\newcommand{\FF}{\mathbf{F}}
\newcommand{\GG}{\mathbf{G}}
\newcommand{\HH}{\mathbf{H}}
\newcommand{\JJ}{\mathbf{J}}
\newcommand{\NN}{\mathbf{N}}
\newcommand{\PP}{\mathbf{P}}
\newcommand{\TT}{\mathbf{T}}
\renewcommand{\SS}{\mathbf{S}}
\newcommand{\ff}{\mathbf{f}}


\newenvironment{smallpmatrix}
{\left(\begin{smallmatrix}}
	{\end{smallmatrix}\right)}

\newenvironment{smallbmatrix}
{\left[\begin{smallmatrix}}
	{\end{smallmatrix}\right]}

\title{Hyperbolic PDEs and switched DAEs}
\author{Raul Borsche, Mauro Garavello, Damla Kocoglu}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}

\section{The Problem}

Consider the coupled problem of switched hyperbolic PDE and switched DAE
\begin{align}
\begin{aligned}
    \partial_t u + A_\sigma(t,x) \partial u &= g_\sigma(t,x,u)\\
    P_\sigma^au(t,a) &= b_\sigma^a(t,w)\\
    P_\sigma^bu(t,b) &= b_\sigma^b(t,w)\\
    u(0,x) &= u_0\\
    E_\sigma\dot{w} &= H_\sigma(t)w+B_\sigma^a(t)u(t,a)+B_\sigma^b(t)u(t,b) + f(t)\\
    w(0) &=w_0
\end{aligned}
\end{align}
$u$ is the solution of the PDE, $w$ the solution of the DAE.
$\sigma(t)\in \mathbf{N}$ is the switching signal


In the first step we consider the problem without switching.
We consider the two problems separately.

\subsection{hyp PDE}

\begin{align}
\begin{aligned}
    \partial_t u + A(t,x) \partial u &= g(t,x,u)\\
    P^au(t,a) &= b^a(t,z)\\
    P^bu(t,b) &= b^b(t,z)\\
    u(0,x) &= u_0\\
\end{aligned}
\end{align}
for a given function $z_a$ and $z_b)$. 

This system admits a unique broad solution (ignoring the boundary see Bressan book, with boundary Falk Hante Diss):
\begin{itemize}
\item $A:\R^+\times [a,b] \rightarrow \R^{n\times n}$ is locally Lipschitz continuous w.r.t. $t$ and $x$.
\item $g:\R^+\times [a,b]\times \R^n \rightarrow \R^{n}$ is locally bounded and measurable w.r.t. $t$ and $x$ and locally Lipschitz continuous w.r.t. $u$.
\item The system is strictly hyperbolic, i.e. $A(t,x)$ has $n$ real and distinc eigenvalues $\lambda_1<\dots<\lambda_n$ and corresponding left $l_i$ and right $r_i$ eigenvectors $i = 1,\dots,n$.
\end{itemize}



\subsection{DAE}
We follow the notation of \cite{DAE_book}

The system
\begin{subequations}
\begin{align}
    \EE \dot{w} &= \HH w + \ff(t) \label{eq:DAE} \\
    w(t_0) &=w_0 \label{eq:DAE_IV}
\end{align}
\end{subequations}
has a unique solution if the following assumptions hold:
$E,A\in \mathbb{R}^{m\times m}$
\begin{enumerate}
	\item The matrix pair $(\EE,\HH)$ is regular, i.e., $\mathrm{det}(s\EE -\HH),\ s\in \R$ is a nonzero polynomial.
    \item $EH=HE$. (This can be omitted using a rescaling of $E,H$ and $f$, i.e., Equation~\eqref{eq:DAE} can be multiplied from the left by $(s\EE-\HH)^{-1}$, then the coefficient matrices $(s\EE-\HH)^{-1}\EE$ and $(s\EE-\HH)^{-1} \HH$ commute while the solution properties remain unchanged.)
%    \item  (for commuting $\EE$ and $\HH$): $\ker E \cap \ker H = \{0\}$
\end{enumerate}
\begin{thm}[\cite{Tren12}]
	For a regular matrix pair $(\EE,\HH)$ with $\EE,\HH \in \R^{m\times m}$, there exist invertible matrices $\SS, \TT \in \R^{m\times m}$ such that $(\EE,\HH)$ is transformed into the \emph{quasi-Weierstraß form}
	\begin{equation} \label{eq:QWF}
		(\SS \EE \TT,\SS \HH \TT) = \begin{smallbmatrix}
		\begin{smallpmatrix}
		\mathbf{I}_{m_1} & \mathbf{0} \\ \mathbf{0} & \NN
		\end{smallpmatrix}, \begin{smallpmatrix}
		\JJ & \mathbf{0} \\ \mathbf{0} & \mathbf{I}{m-m_1}
		\end{smallpmatrix}                         
		\end{smallbmatrix},
	\end{equation}
	where $\JJ\in \R^{m_1 \times m_1}$ is some matrix, $\NN{ (m-m_1) \times (m-m_1)}$ is a nilpotent matrix.
\end{thm}

The quasi-Weierstraß form~\eqref{eq:QWF} decouples the DAE~\eqref{eq:DAE} into ODE $\dot{\mathbf{w}_1} = \JJ \mathbf{w}_1$ and pure DAE $\ \NN \dot{\mathbf{w}_2} = \mathbf{w}_2$.

Let $\nu\in \N_0$ be the nilpotency index of $\NN$, i.e., $\nu$ is the minimum value such that $\NN^\nu = \mathbf{0}$.
Then the solution has the form
\begin{align*}
    w(t) = e^{\EE^D \HH (t-t_0)} \EE^D \EE\bar{w}_0 &+
    \int_{t_0}^t e^{\EE^D \HH(t-s)} \EE^D \ff(s)\,ds\\
    &-\left(\mathbf{I}-\EE^D \EE \right)\sum_{i=0}^{\nu-1}(\EE \HH^D)^i \HH^D \ff^{(i)}(t)
\end{align*}
where $\bar{w}_0$ solves
\begin{align*}
    \bar{w}_0 = E^DE\bar{w}_0-\left(I-E^DE\right)\sum_{i=0}^{\nu-1}(EH^D)^iH^Df^{(i)}(t_0)
\end{align*}
and $A^D$ is the so called Drazin inverse of $A$.
Thus the function $f$ has to be $\nu-1$ times differentiable.
{\bfseries Therefore we will regard only systems of index $\nu = 1$.}

To derive the Drazin inverse, instead of Weierstrass canonical form, where $\JJ$ and $\NN$ are in Jordan form, the\emph{ quasi-Weierstarss form} is employed, where $\JJ$ is some matrix and $\NN$ is nilpotent, cf.~\cite{BergTren12}.


fgreg

Then the solution formula reduces to 
\begin{align*}
    w(t) = e^{E^DH(t-t_0)}E^DE\bar{w}_0 &+
    \int_{t_0}^te^{E^DH(t-s)}E^Df(s)\,ds\\
    &-\left(I-E^DE\right)H^Df(t)
\end{align*}
Thus if $f(t)$ in BV so is $w(t)$.
For a Lipschitz constant we need $t\leq T$?

There exists an alternative formulation of the solution formula, which is slightly more compact but not very known (use notation of Stephan Trenn?)


\section{The coupled system}
Idea of the proof: use Banach fix point theorem.
% \todo{
% \begin{align*}
%     X = &\left(C^0\left([0,T];L^1([a,b];\R^n\right) \cap \left\{TV(u(t,\cdot))\leq M \right\}\cap \left\{|u(t,x)|\leq M \right\}\right)\\
%     &\times
%     \left(C^0([0,T];\R^m)\cap \left\{TV(v)\leq M\right\}\right)
%     \\
%     \Phi&: X \rightarrow X
% \end{align*}

% \begin{align*}
%     \|\Phi(v_1,z_1)-\Phi(v_2,z_2)\|\leq L \left(\|v_1-v_2\|+\|z_1-z_2\|\right)
% \end{align*}

% }

  \bibliographystyle{plain}
  \bibliography{PDE_DAE_BV}

\end{document}
